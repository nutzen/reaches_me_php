<?php

namespace nutzen\reachesme;

class http_utilities {

    public static $verbose_mode = FALSE;

    private static function overflow32($value) {

        $value = $value % 4294967296;
        if ($value > 2147483647) {
            return $value - 4294967296;
        } elseif ($value < -2147483648) {
            return $value + 4294967296;
        } else {
            return $value;
        }
    }

    public static function hash_code($string) {

        $hash = 0;
        $length = strlen($string);
        for ($index = 0; $index < $length; ++$index) {
            $hash = http_utilities::overflow32(31 * $hash + ord($string[$index]));
        }

        return $hash;
    }

// -----------------------------------------------------------------------------

    public static function header_status($statusCode) {
        static $status_codes = null;

        if ($status_codes === null) {
            $status_codes = array(
                100 => 'Continue',
                101 => 'Switching Protocols',
                102 => 'Processing',
                200 => 'OK',
                201 => 'Created',
                202 => 'Accepted',
                203 => 'Non-Authoritative Information',
                204 => 'No Content',
                205 => 'Reset Content',
                206 => 'Partial Content',
                207 => 'Multi-Status',
                300 => 'Multiple Choices',
                301 => 'Moved Permanently',
                302 => 'Found',
                303 => 'See Other',
                304 => 'Not Modified',
                305 => 'Use Proxy',
                307 => 'Temporary Redirect',
                400 => 'Bad Request',
                401 => 'Unauthorized',
                402 => 'Payment Required',
                403 => 'Forbidden',
                404 => 'Not Found',
                405 => 'Method Not Allowed',
                406 => 'Not Acceptable',
                407 => 'Proxy Authentication Required',
                408 => 'Request Timeout',
                409 => 'Conflict',
                410 => 'Gone',
                411 => 'Length Required',
                412 => 'Precondition Failed',
                413 => 'Request Entity Too Large',
                414 => 'Request-URI Too Long',
                415 => 'Unsupported Media Type',
                416 => 'Requested Range Not Satisfiable',
                417 => 'Expectation Failed',
                422 => 'Unprocessable Entity',
                423 => 'Locked',
                424 => 'Failed Dependency',
                426 => 'Upgrade Required',
                500 => 'Internal Server Error',
                501 => 'Not Implemented',
                502 => 'Bad Gateway',
                503 => 'Service Unavailable',
                504 => 'Gateway Timeout',
                505 => 'HTTP Version Not Supported',
                506 => 'Variant Also Negotiates',
                507 => 'Insufficient Storage',
                509 => 'Bandwidth Limit Exceeded',
                510 => 'Not Extended'
            );
        }

        if ($status_codes[$statusCode] !== null) {
            $status_string = $statusCode . ' ' . $status_codes[$statusCode];
            header($status_string, true, $statusCode);
        }
    }

    public static function http_get($url) {

        if (http_utilities::$verbose_mode) {
            echo "<b>GET</b> $url";
        }

        $options = array(
            'http' => array(
                'method' => 'GET',
                'header' => "Content-Type: application/x-www-form-urlencoded"
            )
        );

        $context = stream_context_create($options);
        $response = file_get_contents($url, false, $context);

        return $response;
    }

    public static function http_delete($url) {

        if (http_utilities::$verbose_mode) {
            echo "<b>DELETE</b> $url";
        }

        $options = array(
            'http' => array(
                'method' => 'DELETE',
                'header' => "Content-Type: application/x-www-form-urlencoded"
            )
        );

        $context = stream_context_create($options);
        $response = file_get_contents($url, false, $context);

        return $response;
    }

    public static function http_post($url, $data) {

        $content = json_encode($data); //http_build_query($data);

        if (http_utilities::$verbose_mode) {
            echo "<b>POST</b> $url $content";
        }

        $options = array(
            'http' => array(
                'method' => 'POST',
                'header' => "Content-Type: application/x-www-form-urlencoded",
                'content' => $content
            )
        );

        $context = stream_context_create($options);
        $response = file_get_contents($url, false, $context);

        return $response;
    }

    public static function http_put($url) {

        if (http_utilities::$verbose_mode) {
            echo "<b>PUT</b> $url";
        }

        $options = array(
            'http' => array(
                'method' => 'PUT',
                'header' => "Content-Type: application/x-www-form-urlencoded"
            )
        );

        $context = stream_context_create($options);
        $response = file_get_contents($url, false, $context);

        return $response;
    }

}
