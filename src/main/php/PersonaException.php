<?php

namespace nutzen\reachesme;

class PersonaException extends \Exception {

    private $json;

    public function __construct($json, $message = "", $code = 0, Exception $previous = null) {

        parent::__construct($message, $code, $previous);
        $this->json = $json;
    }

    public function getJson() {

        return $this->json;
    }

}
