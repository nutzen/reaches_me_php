<?php

namespace nutzen\reachesme;

require_once "http_utilities.php";
require_once "PersonaException.php";

class reaches_me_utilities {

    public static function decode_response($response) {

        if (!$response) {
            throw new Exception("No response from server!");
        }

        $json = json_decode($response);

        if (!$json) {
            throw new Exception("Bad JSON in response '$response'");
        } else if ($json->result == "failed") {
            throw new PersonaException($json, "Failure response '$response'");
        }

        return $json;
    }

    public static function handle_persona_exception(PersonaException $exception) {

        $json = $exception->getJson();
        if ($json && ("failed" == $json->result)) {
            if ("unknownuid" == $json->reason) {

                http_utilities::header_status(401);
                die("<br>Unknown WORLD ID\n");
            } else if ("pwderror" == $json->reason) {

                http_utilities::header_status(401);
                die("<br>Incorrect password\n");
            } else if ("exists" == $json->reason) {

                http_utilities::header_status(401);
                die("<br>Object already exists\n");
            }
        }
    }

// -----------------------------------------------------------------------------

    public static function parse_username_available_result($json, $exception = NULL) {


        if ($exception && $json) {
            if (("failed" == $json->result) && ("other" == $json->reason)) {
                throw $exception;
            }
        }

        if ($json) {
            if (("failed" == $json->result) && ("unknownuid" == $json->reason)) {
                return TRUE;
            }
        }

        return FALSE;
    }

    public static function is_username_available($username) {

        $password = "";
        try {

            $json = reaches_me_utilities::get_personalist($username, $password);
            return reaches_me_utilities::parse_username_available_result($json);
        } catch (PersonaException $exception) {

            $json = $exception->getJson();
            return reaches_me_utilities::parse_username_available_result($json, $exception);
        }
    }

// -----------------------------------------------------------------------------

    private static function parse_persona_available_result($json, $exception = NULL) {

        if ($json) {
            if ("available" == $json->result) {
                return TRUE;
            } else if ("exists" == $json->result) {
                return FALSE;
            }
        }

        if ($exception) {
            throw $exception;
        }

        return FALSE;
    }

    public static function is_persona_available($persona) {

        try {

            $json = reaches_me_utilities::check_persona($persona);
            return reaches_me_utilities::parse_persona_available_result($json);
        } catch (PersonaException $exception) {

            $json = $exception->getJson();
            return reaches_me_utilities::parse_persona_available_result($json, $exception);
        }
    }

// -----------------------------------------------------------------------------

    public static function check_persona($persona) {

        // $version = "1.0";
        $type = "checkpersona";
        $message_id = http_utilities::hash_code("$type $persona");

        // $version = urlencode($version);
        $message_id = urlencode($message_id);
        $type = urlencode($type);

        // $username = urlencode($username);
        // $password = urlencode($password);

        $persona = urlencode($persona);

        $url = "https://reaches.me/get?type=$type&msgid=$message_id&persona=$persona";

        $response = http_utilities::http_get($url);

        return reaches_me_utilities::decode_response($response);
    }

    public static function delete_delpersona($username, $password, $persona) {

        $version = "1.0";
        $type = "delpersona";
        $message_id = http_utilities::hash_code("$type $username");

        $version = urlencode($version);
        $message_id = urlencode($message_id);
        $type = urlencode($type);

        $username = urlencode($username);
        $password = urlencode($password);

        $persona = urlencode($persona);

        $url = "https://reaches.me/delete?version=$version&msgid=$message_id&type=$type&uid=$username&pwd=$password&persona=$persona";

        $response = http_utilities::http_delete($url);

        return reaches_me_utilities::decode_response($response);
    }

    public static function delete_delproperty($username, $password, $index) {

        $version = "1.0";
        $type = "delproperty";
        $message_id = http_utilities::hash_code("$type $username");

        $version = urlencode($version);
        $message_id = urlencode($message_id);
        $type = urlencode($type);

        $username = urlencode($username);
        $password = urlencode($password);

        $index = urlencode($index);

        $url = "https://reaches.me/delete?version=$version&msgid=$message_id&type=$type&uid=$username&pwd=$password&idx=$index";

        $response = http_utilities::http_delete($url);

        return reaches_me_utilities::decode_response($response);
    }

    public static function delete_deregister($username, $password) {

        $version = "1.0";
        $type = "deregister";
        $message_id = http_utilities::hash_code("$type $username");

        $version = urlencode($version);
        $message_id = urlencode($message_id);
        $type = urlencode($type);

        $username = urlencode($username);
        $password = urlencode($password);

        $url = "https://reaches.me/delete?version=$version&msgid=$message_id&type=$type&uid=$username&pwd=$password";

        $response = http_utilities::http_delete($url);

        return reaches_me_utilities::decode_response($response);
    }

    public static function get_personalist($username, $password) {

        $version = "1.0";
        $type = "personalist";
        $message_id = http_utilities::hash_code("$type $username");

        $version = urlencode($version);
        $message_id = urlencode($message_id);
        $type = urlencode($type);

        $username = urlencode($username);
        $password = urlencode($password);

        $url = "https://reaches.me/get?version=$version&msgid=$message_id&type=$type&uid=$username&pwd=$password";

        $response = http_utilities::http_get($url);

        return reaches_me_utilities::decode_response($response);
    }

    public static function get_personapropertylist($username, $password, $persona) {

        $version = "1.0";
        $type = "personapropertylist";
        $message_id = http_utilities::hash_code("$type $username");

        $version = urlencode($version);
        $message_id = urlencode($message_id);
        $type = urlencode($type);

        $username = urlencode($username);
        $password = urlencode($password);

        $persona = urlencode($persona);

        $url = "https://reaches.me/get?version=$version&msgid=$message_id&type=$type&uid=$username&pwd=$password&persona=$persona";

        $response = http_utilities::http_get($url);

        return reaches_me_utilities::decode_response($response);
    }

    public static function get_propertylist($username, $password) {

        $version = "1.0";
        $type = "propertylist";
        $message_id = http_utilities::hash_code("$type $username");

        $version = urlencode($version);
        $message_id = urlencode($message_id);
        $type = urlencode($type);

        $username = urlencode($username);
        $password = urlencode($password);

        $url = "https://reaches.me/get?version=$version&msgid=$message_id&type=$type&uid=$username&pwd=$password";

        $response = http_utilities::http_get($url);

        return reaches_me_utilities::decode_response($response);
    }

    public static function get_randomname($username) {

        $version = "1.0";
        $type = "randomname";
        $message_id = http_utilities::hash_code("$type $username");

        $version = urlencode($version);
        $message_id = urlencode($message_id);
        $type = urlencode($type);

//    $username = urlencode($username);

        $url = "https://reaches.me/get?version=$version&msgid=$message_id&type=$type";

        $response = http_utilities::http_get($url);

        return reaches_me_utilities::decode_response($response);
    }

    public static function post_addpersona($username, $password, $persona, $tag) {

        $version = "1.0";
        $type = "addpersona";
        $message_id = http_utilities::hash_code("$type $username");

        $url = "https://reaches.me/post";

        $data = array("version" => $version,
            "msgid" => $message_id,
            "type" => $type,
            "uid" => $username,
            "pwd" => $password,
            "persona" => $persona,
            "tag" => $tag);

        $response = http_utilities::http_post($url, $data);

        return reaches_me_utilities::decode_response($response);
    }

    public static function post_addproperty($username, $password, $persona, $property, $descriptor, $value, $expiry) {

        $version = "1.0";
        $type = "addproperty";
        $message_id = http_utilities::hash_code("$type $username");

        $url = "https://reaches.me/post";

        $data = array("version" => $version,
            "msgid" => $message_id,
            "type" => $type,
            "uid" => $username,
            "pwd" => $password,
            "persona" => $persona,
            "property" => $property,
            "descriptor" => $descriptor,
            "value" => $value,
            "expiry" => $expiry);

        $response = http_utilities::http_post($url, $data);

        return reaches_me_utilities::decode_response($response);
    }

    public static function post_register($username, $password) {

        $version = "1.0";
        $type = "register";
        $message_id = http_utilities::hash_code("$type $username");

        if (!$username || empty($username)) {
            throw new Exception("Invalid username '$username'");
        }

        $url = "https://reaches.me/post";

        $data = array("version" => $version,
            "msgid" => $message_id,
            "type" => $type,
            "uid" => $username,
            "pwd" => $password);

        $response = http_utilities::http_post($url, $data);

        return reaches_me_utilities::decode_response($response);
    }

    public static function put_changeprop($username, $password, $index, $property, $descriptor, $value, $expiry) {

        $version = "1.0";
        $type = "changeprop";
        $message_id = http_utilities::hash_code("$type $username");

        $version = urlencode($version);
        $message_id = urlencode($message_id);
        $type = urlencode($type);

        $username = urlencode($username);
        $password = urlencode($password);

        $index = urlencode($index);
        $property = urlencode($property);
        $descriptor = urlencode($descriptor);
        $value = urlencode($value);
        $expiry = urlencode($expiry);

        $url = "https://reaches.me/put?version=$version&msgid=$message_id&type=$type&uid=$username&pwd=$password&idx=$index&property=$property&descriptor=$descriptor&value=$value&expiry=$expiry";

        $response = http_utilities::http_put($url);

        return reaches_me_utilities::decode_response($response);
    }

    public static function put_changepwd($username, $password, $new_password) {

        $version = "1.0";
        $type = "changepwd";
        $message_id = http_utilities::hash_code("$type $username");

        $version = urlencode($version);
        $message_id = urlencode($message_id);
        $type = urlencode($type);

        $username = urlencode($username);
        $password = urlencode($password);

        $new_password = urlencode($new_password);

        $url = "https://reaches.me/put?version=$version&msgid=$message_id&type=$type&uid=$username&pwd=$password&npwd=$new_password";

        $response = http_utilities::http_put($url);

        return reaches_me_utilities::decode_response($response);
    }

    public static function put_personaaddprop($username, $password, $persona, $index) {

        $version = "1.0";
        $type = "personaaddprop";
        $message_id = http_utilities::hash_code("$type $username");

        $version = urlencode($version);
        $message_id = urlencode($message_id);
        $type = urlencode($type);

        $username = urlencode($username);
        $password = urlencode($password);

        $persona = urlencode($persona);
        $index = urlencode($index);

        $url = "https://reaches.me/put?version=$version&msgid=$message_id&type=$type&uid=$username&pwd=$password&persona=$persona&idx=$index";

        $response = http_utilities::http_put($url);

        return reaches_me_utilities::decode_response($response);
    }

    public static function put_personaremoveprop($username, $password, $persona, $index) {

        $version = "1.0";
        $type = "personaremoveprop";
        $message_id = http_utilities::hash_code("$type $username");

        $version = urlencode($version);
        $message_id = urlencode($message_id);
        $type = urlencode($type);

        $username = urlencode($username);
        $password = urlencode($password);

        $persona = urlencode($persona);
        $index = urlencode($index);

        $url = "https://reaches.me/put?version=$version&msgid=$message_id&type=$type&uid=$username&pwd=$password&persona=$persona&idx=$index";

        $response = http_utilities::http_put($url);

        return reaches_me_utilities::decode_response($response);
    }

    public static function put_rename($username, $password, $new_username) {

        $version = "1.0";
        $type = "rename";
        $message_id = http_utilities::hash_code("$type $username");

        if (!$new_username || empty($new_username)) {
            throw new Exception("Invalid new username '$new_username'");
        }

        $version = urlencode($version);
        $message_id = urlencode($message_id);
        $type = urlencode($type);

        $username = urlencode($username);
        $password = urlencode($password);

        $new_username = urlencode($new_username);

        $url = "https://reaches.me/put?version=$version&msgid=$message_id&type=$type&uid=$username&pwd=$password&nuid=$new_username";

        $response = http_utilities::http_put($url);

        return reaches_me_utilities::decode_response($response);
    }

//PUT
//version=1.0
//type=renamepersona
//msgid
//uid
//pwd
//persona
//newpersona
//newtag
    public static function put_renamepersona($username, $password, $old_persona, $new_persona, $new_tag) {

        $version = "1.0";
        $type = "renamepersona";
        $message_id = http_utilities::hash_code("$type $username");

        if (!$new_persona || empty($new_persona)) {
            throw new Exception("Invalid new persona name '$new_persona'");
        }

        $version = urlencode($version);
        $message_id = urlencode($message_id);
        $type = urlencode($type);

        $username = urlencode($username);
        $password = urlencode($password);

        $old_persona = urlencode($old_persona);
        $new_persona = urlencode($new_persona);
        $new_tag = urlencode($new_tag);

        $url = "https://reaches.me/put?version=$version&msgid=$message_id&type=$type&uid=$username&pwd=$password&persona=$old_persona&newpersona=$new_persona&newtag=$new_tag";

        $response = http_utilities::http_put($url);

        return reaches_me_utilities::decode_response($response);
    }

}
